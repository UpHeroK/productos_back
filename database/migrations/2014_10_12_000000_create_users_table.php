<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('nombres',45);
            $table->string('apellidos',45);
            $table->string('email',45)->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('acerca',200);
            $table->string('password');
            $table->string('telefono');
            $table->rememberToken();

            $table->string('image')->nullable();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
            $table->timestamps();//created_at updated_at
            $table->softDeletes()->nullable();//deleted_at
        
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
