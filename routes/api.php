<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group([
    'middleware' => 'api',
    'prefix' => 'auth'
], function ($router) {
    Route::post('login', 'App\Http\Controllers\AuthController@login')->name('login');
    Route::post('logout', 'App\Http\Controllers\AuthController@logout');
    Route::post('refresh', 'App\Http\Controllers\AuthController@refresh');
    Route::post('register', 'App\Http\Controllers\AuthController@register');
    Route::post('me', 'App\Http\Controllers\AuthController@me');
    Route::get('listarusers', 'App\Http\Controllers\AuthController@index');
    Route::get('traeruser/{id} ', 'App\Http\Controllers\AuthController@show');
    Route::put('updateuser/{id} ', 'App\Http\Controllers\AuthController@update');
    Route::delete('deleteuser/{id} ', 'App\Http\Controllers\AuthController@destroy');

    //atributos
    Route::post('registrar-atributo','App\Http\Controllers\AtributoController@store');
    Route::get('listar-atributos','App\Http\Controllers\AtributoController@index');
    Route::put('actualizar-atributo/{id}','App\Http\Controllers\AtributoController@update');
    Route::delete('eliminar-atributo/{id}','App\Http\Controllers\AtributoController@destroy');

    // categorias
    Route::post('registrar-categoria','App\Http\Controllers\CategoriaController@store');
    Route::get('listar-categorias','App\Http\Controllers\CategoriaController@index');
    Route::put('actualizar-categoria/{id}','App\Http\Controllers\CategoriaController@update');
    Route::delete('eliminar-categoria/{id}','App\Http\Controllers\CategoriaController@destroy');

    //variables 
    Route::post('registrar-variable','App\Http\Controllers\VariableController@store');
    Route::get('listar-variables','App\Http\Controllers\VariableController@index');
    Route::put('actualizar-variable/{id}','App\Http\Controllers\VariableController@update');
    Route::delete('eliminar-variable/{id}','App\Http\Controllers\VariableController@destroy');
});
