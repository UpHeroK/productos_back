<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Models\User;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{
    /**
     * Create a new AuthController instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login()
    {
        $credentials = request(['email', 'password']);

        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        return $this->respondWithToken($token);
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function me()
    {
        return response()->json(auth()->user());
    }

    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'Successfully logged out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->respondWithToken(auth()->refresh());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth()->factory()->getTTL() * 60
        ]);
    }
    public function register(Request $request)
    {
        $validator=Validator::make($request->all(),[

            'image'=>'required',
            'nombres' =>'required',
            'apellidos' => 'required',
            'email' =>'required|string|email|max:30',
            'password' =>'required',
            'acerca'=>'required|string|max:500',
            'telefono'=>'required|string|max:20',
           
        ]);
        $image_64 = $request['image']; //your base64 encoded data
                $extension = explode('/', explode(':', substr($image_64, 0, strpos($image_64, ';')))[1])[1];   // .jpg .png .pdf   
                $replace = substr($image_64, 0, strpos($image_64, ',') + 1);
                // find substring fro replace here eg: data:image/png;base64,   
                $image = str_replace($replace, '', $image_64);
                $image = str_replace(' ', '+', $image);
                $imageName = Str::random(10) . '.' . $extension;
                $imagen = Storage::url($imageName);
                $url_img = Storage::disk('public')->put($imageName, base64_decode($image));
        

        if($validator->fails()){
            return response()->json($validator->errors()->toJson(),400);
        }
        $user= User::create(array_merge(
            $validator->validate(),
            [
                'password'=>bcrypt($request->password),
                'image' => $imagen
            ]
        ));
        return response()->json([
            'message'=>'¡Usuario registrado correctamente!',
            'user'=>$user,
        ],201);

    }
    public function index()
    {
        try {
            $usuarios = User::get();
            foreach ($usuarios as $usuario) {
                $usuario->image = env('APP_URL'). $usuario->image;
            }
            return $usuarios;
        } catch (\Throwable $th) {
            return $th;
        }
    }
    //listar 1 admin
    public function show($id)
    {
       try {
        $usuario = User::find($id);
        $usuario->image = env('APP_URL'). $usuario->image;
        return $usuario;
       } catch (\Throwable $th) {
           throw $th;
       }
    }
    //Actualizar info de 1 admin
    public function update(int $id, Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'image' => 'required',
                'nombres' => 'required',
                'apellidos' => 'required',
                'email' => 'required|string|email|max:100',
                'password' => 'required|min:6',
                'acerca' => 'required|string|max:500',
                'telefono' => 'required|string|min:7|max:10'
            ]);
            if($validator->fails()){
                return response()->json($validator->errors()->toJson(),400);
            }
            $usuario = User::findOrFail($id);
            $usuario = $usuario->update([
                'imagen' => $request->image,
                'acerca' => $request->acerca,
                'apellidos' => $request->apellidos,
                'email' => $request->email,
                'nombres' => $request->nombres,
                'password' => $request->password,
                'telefono' => $request->telefono
            ]);
            return response()->json([
                'message' => 'Datos actualizados correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    //Eliminar 1 admin
    public function destroy($id)
    {
        try {
            //return "id a borrar: ".$id;
            $usuario = User::findOrFail($id);
            //return $usuario;
            $usuario= $usuario->delete();
            return response()->json([
                'message' => 'Usuario eliminado correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }


    

}

