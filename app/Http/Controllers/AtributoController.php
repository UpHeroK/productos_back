<?php

namespace App\Http\Controllers;

use App\Models\Atributo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AtributoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $atributos = Atributo::with('variables')->withCount('variables')->get()->toArray();
            // $atributos = Atributo::with('variables')->get();
            return $atributos;  
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nombreatributo' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 400);
            }
            $atributo = Atributo::create(array_merge(
                $validator->validate(),
                [
                    'nombreatributo' => $request->nombreatributo
                ]
            ));
            return response()->json([
                'message' => '¡Atributo registrado correctamente',
                'atributo' => $atributo,
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Atributo  $atributo
     * @return \Illuminate\Http\Response
     */
    public function show(Atributo $atributo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Atributo  $atributo
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombreatributo' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $atributo = Atributo::findOrFail($id);
        $atributo = $atributo->update([
            'nombreatributo' => $request->nombreatributo,

        ]);
        return response()->json([
            'message' => '¡Atributo actualizado correctamente',
            'Atributo' => $atributo,
            'successfull' => true
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Atributo  $atributo
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try {
            //return "id a borrar: ".$id;
            $atributo = Atributo::findOrFail($id);
            //return $usuario;
            $atributo = $atributo->delete();
            return response()->json([
                'message' => 'atributo eliminado correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
