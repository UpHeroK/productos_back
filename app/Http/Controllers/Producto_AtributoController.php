<?php

namespace App\Http\Controllers;

use App\Models\ProductoAtributo;
use Illuminate\Http\Request;

class ProductoAtributoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Producto_Atributo  $producto_Atributo
     * @return \Illuminate\Http\Response
     */
    public function show(ProductoAtributo $producto_Atributo)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Producto_Atributo  $producto_Atributo
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductoAtributo $producto_Atributo)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Producto_Atributo  $producto_Atributo
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductoAtributo $producto_Atributo)
    {
        //
    }
}
