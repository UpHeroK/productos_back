<?php

namespace App\Http\Controllers;

use App\Models\ProductoImagen;
use Illuminate\Http\Request;

class ProductoImagenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductoImagen  $productoImagen
     * @return \Illuminate\Http\Response
     */
    public function show(ProductoImagen $productoImagen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductoImagen  $productoImagen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductoImagen $productoImagen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductoImagen  $productoImagen
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductoImagen $productoImagen)
    {
        //
    }
}
