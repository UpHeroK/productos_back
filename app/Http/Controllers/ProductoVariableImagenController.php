<?php

namespace App\Http\Controllers;

use App\Models\ProductoVariableImagen;
use Illuminate\Http\Request;

class ProductoVariableImagenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductoVariableImagen  $productoVariableImagen
     * @return \Illuminate\Http\Response
     */
    public function show(ProductoVariableImagen $productoVariableImagen)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductoVariableImagen  $productoVariableImagen
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductoVariableImagen $productoVariableImagen)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductoVariableImagen  $productoVariableImagen
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductoVariableImagen $productoVariableImagen)
    {
        //
    }
}
