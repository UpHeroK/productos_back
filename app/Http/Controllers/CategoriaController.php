<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;

class CategoriaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $categorias = Categoria::get();
            foreach ($categorias as $categoria) {
                $categoria->imagencategoria = env('APP_URL') . $categoria->imagencategoria;
            }
            return $categorias;
        } catch (\Throwable $th) {
            return $th;
        }
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'nombreCategoria' => 'required',
                'imagenCategoria' => 'required',
                'cantidad' => 'required'
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 400);
            }
            $categoria = Categoria::create(array_merge(
                $validator->validate(),
                [
                    'nombrecategoria' => $request->nombreCategoria,
                    'imagencategoria' => $this->convertirimg($request->imagenCategoria),
                    'cantidad' => $request->cantidad,
                ]
            ));
            return response()->json([
                'message' => '¡Categoria registrado correctamente',
                'Categoria' => $categoria,
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function show(Categoria $categoria)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function update(int $id, Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombrecategoria' => 'required',
            'imagencategoria'=> 'required',
            'cantidad'=> 'required'
        ])
        ;
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        
        $categoria = Categoria::findOrFail($id);
        $categoria = $categoria->update([
            'nombrecategoria' => $request->nombrecategoria,
            'imagencategoria' => $this->convertirimg($request->imagencategoria),
            'cantidad' => $request->cantidad,

        ]);
        return response()->json([
            'message' => '¡Categoria actualizada correctamente',
            'successfull' => true
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categoria  $categoria
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $id)
    {
        try {
            //return "id a borrar: ".$id;
            $categoria = Categoria::findOrFail($id);
            //return $usuario;
            $categoria = $categoria->delete();
            return response()->json([
                'message' => 'Categoria eliminada correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
