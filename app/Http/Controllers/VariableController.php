<?php

namespace App\Http\Controllers;

use App\Models\Variable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class VariableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        try {
            $variables = Variable::get();
            // $atributos = Atributo::with('variables')->get();
            return $variables;
        } catch (\Throwable $th) {
            return $th;
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'valor' => 'required',
                'atributo_id' => 'required',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->errors()->toJson(), 400);
            }
            $variable = Variable::create(array_merge(
                $validator->validate(),
                [
                    'valor' => $request->valor,
                    'atributo_id' => $request->atributo_id
                ]
            ));
            return response()->json([
                'message' => 'variable registrada correctamente',
                'variable' => $variable,
                'successfull' => true
            ], 200);
        } catch (\Throwable $th) {
            throw $th;
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function show(Variable $variable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function update(int $id,Request $request)
    {
        $validator = Validator::make($request->all(), [
            'valor' => 'required',
        ]);
        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $variable = Variable::findOrFail($id);
        $variable = $variable->update([
            'valor' => $request->valor,

        ]);
        return response()->json([
            'message' => 'variable actualizada correctamente',
            'Variable' => $variable,
            'successfull' => true
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Variable  $variable
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            //return "id a borrar: ".$id;
            $variable = Variable::findOrFail($id);
            //return $usuario;
            $variable = $variable->delete();
            return response()->json([
                'message' => 'variable eliminada correctamente',
                'successfull' => true
            ], 201);
        } catch (\Throwable $th) {
            throw $th;
        }
    }
}
