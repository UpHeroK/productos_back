<?php

namespace App\Http\Controllers;

use App\Models\ProductoCantidadPrecio;
use Illuminate\Http\Request;

class ProductoCantidadPrecioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ProductoCantidadPrecio  $productoCantidadPrecio
     * @return \Illuminate\Http\Response
     */
    public function show(ProductoCantidadPrecio $productoCantidadPrecio)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ProductoCantidadPrecio  $productoCantidadPrecio
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ProductoCantidadPrecio $productoCantidadPrecio)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ProductoCantidadPrecio  $productoCantidadPrecio
     * @return \Illuminate\Http\Response
     */
    public function destroy(ProductoCantidadPrecio $productoCantidadPrecio)
    {
        //
    }
}
