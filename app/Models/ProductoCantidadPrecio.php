<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductoCantidadPrecio extends Model
{
    use HasFactory;

    protected $table ="productos_cantidades_precios";

    protected $fillable =[
        'cantidad',
        'precio',
        
    ];

    

}
