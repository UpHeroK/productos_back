<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Atributo;
use App\Models\Categoria;
class Producto extends Model
{
    use HasFactory;
    protected $table ="productos";

    protected $fillable =[
        'image',
        'nombre',
        'precio',
        'descripcion'
    ];

    // Producto - categoria
    public function categoria(){
        return $this->belongsTo(Categoria::class , 'categoria_id');
    }

    public function atributos(){
        return $this->belongsToMany(Atributo::class);
    }
}
