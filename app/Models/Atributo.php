<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Producto;
use App\Models\Variable;

class Atributo extends Model
{
    use HasFactory;
    protected $table ="atributos";

    protected $fillable =[
        'nombreatributo'
        
    ];

    // atributos - producto
    
    public function productos(){
        return $this->belongsToMany(Producto::class);
    }

    public function variables(){
        return $this->hasMany(Variable::class);
    }
  
  
}