<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Atributo;

class Variable extends Model
{
    use HasFactory;
    protected $table = "variables";

    protected $fillable=[
        'valor',
        'atributo_id'
    ];
    public function atributo()
    {
        return $this->belongsTo(Atributo::class);
    }
}
